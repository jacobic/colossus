{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Colossus tutorial: MCMC fitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Colossus includes a basic MCMC fitting module based on the [Goodman & Weare 2010](http://adsabs.harvard.edu/abs/2010CAMCS...5...65G) algorithm, contributed by Andrey Kravtsov. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function \n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to define a likelihood function which we are trying to maximize. For a quick demonstration, let's use a double Gaussian with correlated parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def likelihood(x):\n",
    "\n",
    "    sig1 = 1.0\n",
    "    sig2 = 2.0\n",
    "    r = 0.95\n",
    "    r2 = r * r\n",
    "    res = np.exp(-0.5 * ((x[:, 0] / sig1)**2 + (x[:, 1] / sig2)**2 - 2.0 * r * x[:, 0] * x[:, 1] \\\n",
    "            / (sig1 * sig2)) / (1.0 - r2)) / (2 * np.pi * sig1 * sig2) / np.sqrt(1.0 - r2)\n",
    "\n",
    "    return res"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running the MCMC is easy now: we need to decide on an initial guess for the parameters and a number of \"walkers\" (chains run in parallel). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "param_names = ['x1', 'x2']\n",
    "x_initial = np.array([1.0, 1.0])\n",
    "n_params = len(param_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could just use the `run()` function to complete all the following steps in one function call, but for the sake of demonstration, let's break it down into the main steps.\n",
    "\n",
    "First, the `runChain()` function does the actual MCMC sampling. It takes more optional arguments than shown in the code below. By default, the MCMC is stopped when the Gelman-Rubin criterion is below a certain number in all parameters. Running this code should take less than a minute on a modern laptop. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running MCMC with the following settings:\n",
      "Number of parameters:                      2\n",
      "Number of walkers:                       200\n",
      "Save conv. indicators every:             100\n",
      "Finish when Gelman-Rubin less than:   0.0100\n",
      "-------------------------------------------------------------------------------------\n",
      "Step    100, autocorr. time  28.7, GR = [  1.318  1.323]\n",
      "Step    200, autocorr. time  51.1, GR = [  1.131  1.138]\n",
      "Step    300, autocorr. time  51.7, GR = [  1.086  1.090]\n",
      "Step    400, autocorr. time  52.7, GR = [  1.063  1.068]\n",
      "Step    500, autocorr. time  50.8, GR = [  1.049  1.055]\n",
      "Step    600, autocorr. time  49.5, GR = [  1.040  1.046]\n",
      "Step    700, autocorr. time  48.6, GR = [  1.035  1.039]\n",
      "Step    800, autocorr. time  47.3, GR = [  1.033  1.037]\n",
      "Step    900, autocorr. time  45.9, GR = [  1.029  1.033]\n",
      "Step   1000, autocorr. time  44.7, GR = [  1.025  1.028]\n",
      "Step   1100, autocorr. time  42.0, GR = [  1.023  1.026]\n",
      "Step   1200, autocorr. time  41.7, GR = [  1.021  1.023]\n",
      "Step   1300, autocorr. time  41.1, GR = [  1.021  1.022]\n",
      "Step   1400, autocorr. time  40.6, GR = [  1.020  1.021]\n",
      "Step   1500, autocorr. time  40.5, GR = [  1.020  1.020]\n",
      "Step   1600, autocorr. time  41.5, GR = [  1.018  1.019]\n",
      "Step   1700, autocorr. time  40.5, GR = [  1.017  1.018]\n",
      "Step   1800, autocorr. time  41.3, GR = [  1.016  1.017]\n",
      "Step   1900, autocorr. time  40.4, GR = [  1.016  1.017]\n",
      "Step   2000, autocorr. time  39.5, GR = [  1.015  1.016]\n",
      "Step   2100, autocorr. time  39.3, GR = [  1.014  1.014]\n",
      "Step   2200, autocorr. time  38.9, GR = [  1.013  1.013]\n",
      "Step   2300, autocorr. time  38.1, GR = [  1.012  1.012]\n",
      "Step   2400, autocorr. time  38.0, GR = [  1.012  1.012]\n",
      "Step   2500, autocorr. time  38.1, GR = [  1.011  1.012]\n",
      "Step   2600, autocorr. time  37.3, GR = [  1.011  1.011]\n",
      "Step   2700, autocorr. time  36.7, GR = [  1.010  1.010]\n",
      "Step   2800, autocorr. time  35.6, GR = [  1.010  1.010]\n",
      "Step   2900, autocorr. time  35.7, GR = [  1.010  1.010]\n",
      "-------------------------------------------------------------------------------------\n",
      "Acceptance ratio:                          0.661\n",
      "Total number of samples:                  580000\n",
      "Samples in burn-in:                       140000\n",
      "Samples without burn-in (full chain):     440000\n",
      "Thinning factor (autocorr. time):             35\n",
      "Independent samples (thin chain):          12572\n"
     ]
    }
   ],
   "source": [
    "from colossus.utils import mcmc\n",
    "\n",
    "walkers = mcmc.initWalkers(x_initial, nwalkers = 200, random_seed = 156)\n",
    "chain_thin, chain_full, _ = mcmc.runChain(likelihood, walkers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the chain output, we can now compute the most likely values for the parameters as well as confidence intervals. We use the thinned chain for this purpose because the full chain's individual samples are highly correlated, leading to erroneous statistical inferences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------------------------------------------\n",
      "Statistics for parameter 0, x1:\n",
      "Mean:              -5.000e-03\n",
      "Median:            -1.595e-02\n",
      "Std. dev.:         +9.745e-01\n",
      "68.3% interval:    -9.792e-01 .. +9.680e-01\n",
      "95.5% interval:    -1.925e+00 .. +1.986e+00\n",
      "99.7% interval:    -2.819e+00 .. +3.151e+00\n",
      "-------------------------------------------------------------------------------------\n",
      "Statistics for parameter 1, x2:\n",
      "Mean:              -1.308e-02\n",
      "Median:            -1.386e-02\n",
      "Std. dev.:         +1.952e+00\n",
      "68.3% interval:    -1.972e+00 .. +1.924e+00\n",
      "95.5% interval:    -3.925e+00 .. +3.972e+00\n",
      "99.7% interval:    -5.824e+00 .. +5.848e+00\n"
     ]
    }
   ],
   "source": [
    "mcmc.analyzeChain(chain_thin, param_names = param_names);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To elucidate the individual and joint likelihood distributions of the parameters, it is helpful to plot the chain output. The following function does just that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.colors import LogNorm\n",
    "import matplotlib.gridspec as gridspec\n",
    "\n",
    "def plotChain(chain, param_labels):\n",
    "    \"\"\"\n",
    "    Plot a summary of an MCMC chain.\n",
    "\n",
    "    This function creates a triangle plot with a 2D histogram for each combination of parameters,\n",
    "    and a 1D histogram for each parameter. The plot is not automatically saved or shown, the user\n",
    "    can determine how to use the plot after executing this function.\n",
    "\n",
    "    Parameters\n",
    "    -----------------------------------------------------------------------------------------------\n",
    "    chain: array_like\n",
    "        A numpy array of dimensions ``[nsteps, nparams]`` with the parameters at each step in the \n",
    "        chain. The chain is created by the :func:`runChain` function.\n",
    "    param_labels: array_like\n",
    "        A list of strings which are used when plotting the parameters. \n",
    "    \"\"\"\n",
    "\n",
    "    nsamples = len(chain)\n",
    "    nparams = len(chain[0])\n",
    "\n",
    "    # Prepare panels\n",
    "    margin_lb = 1.0\n",
    "    margin_rt = 0.5\n",
    "    panel_size = 2.5\n",
    "    size = nparams * panel_size + margin_lb + margin_rt\n",
    "    fig = plt.figure(figsize = (size, size))\n",
    "    gs = gridspec.GridSpec(nparams, nparams)\n",
    "    margin_lb_frac = margin_lb / size\n",
    "    margin_rt_frac = margin_rt / size\n",
    "    plt.subplots_adjust(left = margin_lb_frac, bottom = margin_lb_frac, right = 1.0 - margin_rt_frac,\n",
    "                    top = 1.0 - margin_rt_frac, hspace = margin_rt_frac, wspace = margin_rt_frac)\n",
    "    panels = [[None for dummy in range(nparams)] for dummy in range(nparams)] \n",
    "    for i in range(nparams):\n",
    "        for j in range(nparams):\n",
    "            if i >= j:\n",
    "                pan = fig.add_subplot(gs[i, j])\n",
    "                panels[i][j] = pan\n",
    "                if i < nparams - 1:\n",
    "                    pan.set_xticklabels([])\n",
    "                else:\n",
    "                    plt.xlabel(param_labels[j])\n",
    "                if j > 0:\n",
    "                    pan.set_yticklabels([])\n",
    "                else:\n",
    "                    plt.ylabel(param_labels[i])\n",
    "            else:\n",
    "                panels[i][j] = None\n",
    "\n",
    "    # Plot 1D histograms\n",
    "    nbins = min(50, nsamples / 20.0)\n",
    "    minmax = np.zeros((nparams, 2), np.float)\n",
    "    for i in range(nparams):\n",
    "        ci = chain[:, i]\n",
    "        plt.sca(panels[i][i])\n",
    "        _, bins, _ = plt.hist(ci, bins = nbins)\n",
    "        minmax[i, 0] = bins[0]\n",
    "        minmax[i, 1] = bins[-1]\n",
    "        diff = minmax[i, 1] - minmax[i, 0]\n",
    "        minmax[i, 0] -= 0.03 * diff\n",
    "        minmax[i, 1] += 0.03 * diff\n",
    "        plt.xlim(minmax[i, 0], minmax[i, 1])\n",
    "\n",
    "    # Plot 2D histograms\n",
    "    for i in range(nparams):\n",
    "        ci = chain[:, i]\n",
    "        for j in range(nparams):\n",
    "            cj = chain[:, j]\n",
    "            if i > j:\n",
    "                plt.sca(panels[i][j])\n",
    "                plt.hist2d(cj, ci, bins = 100, norm = LogNorm(), normed = 1)\n",
    "                plt.ylim(minmax[i, 0], minmax[i, 1])\n",
    "                plt.xlim(minmax[j, 0], minmax[j, 1])\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function is not part of the main body of Colossus because it relies on matplotlib. Here is its output for the chain above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAawAAAGeCAYAAADIejUKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMS4xLCBo\ndHRwOi8vbWF0cGxvdGxpYi5vcmcvAOZPmwAAIABJREFUeJzt3WuMnNd5J/j/U9e+d7G7ixRJXaim\nEs/K8UQpti0r8mq1o6Y0QsYLeEVKRpDBfErJE2CxiOmVAviD4A/CiIzsYBEsNizNADvIYNeK6JkP\n+eCRSe4YWju241ZHmWTkicbdoiVRItW36lt1ddfl2Q/Peaurm81m9e2t9636/4BGVZ16q+o0u1hP\nPed9zjmiqiAiIgq6SLM7QERE1AgGLCIiCgUGLCIiCgUGLCIiCgUGLCIiCgVfA5aIjLqf83Vt591l\ntq7tjDvuhZ22ERFRa/ItYIlIBsBpVb0CIONuA0BWRCYATNYdB3dcXkQyjbb59bsQEZH/Yn69kKqO\nAxh3N4fdbQA464KO5zkAl931SQCjAAYbbBtHA4aGhvTEiRO7+C2AqakppNPpXT32IAStP0Dw+hS0\n/gC769Pbb789rarB+kWIfORbwPK44bvn65oyIgIAGVW9ACAFYLbu/sEdtDXkxIkTGBsb22HPzcjI\nyK4fexCC1h8geH0KWn+A3fVJRH51QN0hCgXfiy5cUHpeRFLebZdhDYrIqB99mJqawsjISO0nl8v5\n8bJEO5bL5WrvUwBDze4PUTP5lmHVnXMahw3hZUVk0rVdAjADYBhAHsCAe1jKtWMHbXeUTqcD942b\naCvZbBbZrNUjich0k7tD1FR+DgnWn2NKAfg5LHBNuraTAC4CGAMw4tqGAXjntxptO1Deh0dQBK0/\nQPD6FLT+AMHsE1HQiV+L37ohwGdh551Oq+rzrj3r2obdcKHXNunacjtpa8TIyIgyw6KwEZG3VXXk\nzkcStSbfAlaQMGBRGDFgUbvjShdERBQKDFhERBQKvs/DotYm8mrtuuo3mtgTImo1zLCIiCgUGLCI\niCgUOCRIe1Y/DEhEdFCYYRERUSgwYBERUSgwYBERUSgwYBERUSiw6IIOzOZiDM7LIqK9YIZFRESh\nwIBFREShwIBFREShwHNYtGOcKExEzcAMi4iIQsHXDEtERt3V06r6oms7AyAPIFO34/Cu24iIqDX5\nlmGJSAYWqK4AyIhIxrXBteX32ubX70JERP7zLWCp6riXVQEYVtVxAM/BMiQAmAQwusc2IiJqUb6f\nwxKRFwA8726mAMzW3T24xzYiImpRvgcsd67peRFJ+f3anqmpKYyMjNR+crlcs7pCtK1cLld7nwIY\nanZ/iJrJt6KLunNO47AhvCxsSG/AHZICMOOu76XtjtLpNMbGxnb4GxD5L5vNIpvNAgBEZLrJ3SFq\nKj+rBEcBjLvrKQA/B3AFwIhrG3a3scc2Cqj6+VtcV5CIdsrPIcEcgGFXig5VveSyLa/cPe8KM3bd\n5uPvQkREPvMtw1LVPCxoAcCluvZbTiDtpY2IiFoTV7ogIqJQYMAiIqJQYMAiIqJQYMAiIqJQYMAi\nIqJQ4H5Y1BDugUVEzcYMi4iIQoEBi4iIQoEBi4iIQoEBi4iIQoEBi4iIQoEBi4iIQoFl7dQU3GqE\niHaKGRYREYUCAxYREYWCrwFLRLLu53xd23nvvrq2MyIyKiIv7LSNiIhak28By+0MfMVtujjsbgNA\nVkQmAEy64zIAoKpXAORFJNNom1+/CxER+c/PDGsYgBekJt1tADirqidd4AGA5wDk644b3UEbERG1\nKN+qBDdtZ58B8Lp3XUQAIKOqFwCkAMzWHTu4gzYiImpRvpe1u6G7y6o6DgAuSEFETtcNEx6oqakp\njIyM1G5ns1lks9ltHkHUHLlcDrlc7bveUDP7QtRszZiHNVoXpM4AgKpeAjADGybMAxhwx6ZcO3bQ\ndkfpdBpjY2O77T+Rb+q/TInIdJO7Q9RUvgYsEcnWBatR2LmnSXf3SQAXAYwB8NKfYQDeua1G22if\ncA8sIgoSv6sEz4vIhIjMAYAbFnzWZVoTqjruDRW64/M7afPrdyEiIv/5WXRxBcChLdpz+9lGRESt\niStdEBFRKDBgERFRKDBgERFRKDBgERFRKHA/LGo67o1FRI1ghkVERKHAgEVERKHAgEVERKHAgEVE\nRKHAgEVERKHAgEVERKHAgEVERKHAgEVERKHAgEVERKHAlS5oA27aSERBxQyLiIhCwdcMS0Sy7upJ\nVX3RtZ0BkAeQUdULe20jIqLW5FvAclvZX1HVSRF5w92eBWw3YhEZFpGMd/xu2lR13K/fh4iI/OXn\nkOAwgFF3fdLdfg6WIXlto3tsIyKiFuVbhqWqubqbGQCvAzgFl2U5gwBSe2gjIqIW5XuVoBvOu6yq\n4yLi98sDAKampjAyMlK7nc1mkc1mt3kEUXPkcjnkcrXvekPN7AtRszWjrH20rkAiD2DAXU8BmHHX\n99J2R+l0GmNjYzvsNpH/6r9Mich0k7tD1FS+VwnWVfiNwoYFvVRnGMAVd30vbRRim+eBcQdiIvL4\nVnThAtR5EZkQkTkA8Kr63H15VR3fS5tfvwsREfnPz6KLKwAObdGe2882IiJqTVzpgoiIQoEBi4iI\nQoEBi4iIQmHbc1gictsSLVXlst5EROSbO2VYCwDOw+ZLzW/6ISIi8s22GZaq5kTkjKr+a786RERE\ntJU7nsNS1Sf96AgREdF2GpqHJSL/RFX/37rb/7Oq/vuD6xb5hTsME1FYNFol+DUR+Vci0icifwHg\n9EF2ioiIaLOGApaqPgtAAMwB+GtV/ZcH2isiIqJNGgpYIvIKgPsBPADgye3K3YmIiA5Co0OCE6r6\nnKq+74owmrORFRERta2Gii5U9bVNt//4YLpDtFF9UQi3GiFqb1yaiYiIQsH3gCUimU23z7vLbF3b\nGREZFZEXdtpGREStydeA5TZbfG1Tc1ZEJgBMumMyQG3/rLyIZBpt8+v3ICIi//kasFxwmd3UfFZV\nT7r7AOA52NqFgAWx0R20ERFRi/Jtx+FtZEQEADKqegFAChuD2uAO2oiIqEU1PWC5IAUROe2GDImI\niG7R1IAlImcAQFUvAZgBMAwb5htwh6RcO3bQdkdTU1MYGRmp3c5ms8hms9s8gqg5crkccrmcd3Oo\nmX0harZmZ1iT7gcATgK4CGAMgBdNhgF457YabbujdDqNsbGxXXaZyD/1X6ZEZLrJ3SFqKl8Dlsuo\nRtweW5dUdVxEsiIyC1tNY9wdN+KGB/M7baM74wrtRBRGvgYsN/R3aVNbbovjdt1GREStiStdEBFR\nKDBgERFRKDBgERFRKDBgERFRKDS7rJ2oYdxqhKi9McMiIqJQYMAiIqJQYMAiIqJQYMAiIqJQYMAi\nIqJQYMAiIqJQYMAiIqJQYMAiIqJQ4MThNsEtRYgo7JhhERFRKDBgERFRKPgesEQks+n2GREZFZEX\n9qONiIhak68By21n/1rd7QwAqOoVAHkRyeylzc/fhYiI/OVrwHLBZbau6TkAeXd9EsDoHtuIiKhF\nNbtKMIWNAWxwj23UJrjVCFH7YdEFERGFQrMzrDyAAXc9BWDGXd9L2x1NTU1hZGSkdjubzSKbze6o\n40R+yOVyyOVy3s2hZvaFqNmaHbBeB+BFjmEAV9z1vbTdUTqdxtjY2G76S+Sr+i9TIjLd5O4QNZXf\nVYJnAIy4S6jquGsfBZBX1fG9tPn5uxARkb98zbBU9RKAS5vaclsct+s2IiJqTSy6ICKiUGDAIiKi\nUGh20QUdIK7QTkSthBkWERGFAgMWERGFAgMWERGFAgMWERGFAosuKPQ2F5dwMVyi1sQMi4iIQoEB\ni4iIQoEBi4iIQoEBi4iIQoEBi4iIQoFVgi2ESzERUStjhkVERKHAgEVERKHQ9IAlIufdZbau7YyI\njIrICzttIyKi1tT0gAUgKyITACYBQEQyAKCqVwDkRSTTaFtzuk9BI/Jq7YeIWkcQAtZZVT3pAg8A\nPAcg765PAhjdQRsREbWoIFQJZkQEADKqegFACsBs3f2DO2gjIqIW1fSA5YIUROS0iPiSJU1NTWFk\nZKR2O5vNIpvNbvMIoubI5XLI5XLezaFm9oWo2ZoasETkDACo6iUAMwCGYcN8A+6QlGvHDtruKJ1O\nY2xsbPcdJ/JJ/ZcpEZlucneImqrZGdak+wGAkwAuAhgD4KU/wwC8c1uNtrUVFhYQUbtoasBS1XER\nyYrILIAJVR0HABEZccOD+Z22ERFRa2p2hgVVze1nGxERtaamByyig1Q/ZMqdiInCLQjzsIiIiO6I\nAYuIiEKBAYuIiEKB57BCiKXsRNSOmGEREVEoMMOitrE5M2XVIFG4MMMiIqJQYMAiIqJQ4JBgCLDI\ngoiIGRYREYUEMyxqW1y2iShcmGEREVEoMMMKKJ63IiLaiAGLCBweJAoDDgkSEVEohDrDEpEzAPIA\nMqp6odn92SsOAwYDsy2iYApthiUiGQBQ1SsA8t7tg5bL7e8mxyKv1n5256f72p/9EbQ+Ba0/+/8+\nImoHoQ1YAJ6DZVcAMAlg1I8X3esHTX2A2p+M6mf78Bz7LWh92n1/9v/vZRiwiHYuzEOCKQCzdbcH\nm9URD4f0Wt9u/sYcViTaH6Kqze7DrojIRQAXVXVcREYBnFbVFxt87CI2ZpdTAKYbfOmhHRzrh6D1\nBwhen4LWH6DxPg0BSLvrVVXtPbguEQVbmDOsPIABdz0FYKbRB/I/PRFR+IT5HNbrAIbd9WEAV5rY\nFyIiOmChDViqOg4Abjgw790mIqLWFNpzWERE1F5Cm2EREVF7YcAiIqJQYMAiIqJQYMAiIqJQYMAi\nIqJQYMAiIqJQYMAiIqJQYMAiIqJQCPNagrs2NDSkJ06c2NVjp6amkE6n73ygT4LWHyB4fQpaf4Dd\n9entt9+eVtUdPWgv7/WDFMS/yU6Euf9B7/t27/O2DFgnTpzA2NjYrh47MjKy68cehKD1Bwhen4LW\nH2B3fRKRX+30dfbyXj9IQfyb7ESY+x/0vm/3PueQIBERhQIDFhERhQID1g5ls9lmd2GDoPUHCF6f\ngtYf4PZ9Oh05i9ORsz73xn9B/JvsRJj7H+a+t+Vq7SMjIxrkMVxqX16wulx945b7RORtVR3ZyfPx\nvU5hs937vC2LLoiCaqtARUSGQ4JERBQKzLCIiBog8uqG26rfaFJP2hcDFhHRLtQHMAYvf3BIkIiI\nQoEBi+iAPJV5CU9lXtryvnYoXSfabwxYRAek+s67qL7zbsPH3y64EZEJdMASkYyIqIhMuJ+LWxxz\n3l2GdzYcERHdUdCLLgZUVQALXgDyWxyTFZEzAJ73tWdEW3jisZcBAFff+ua2c6q2uu/N8W8dWL9o\ndzZXBlJzBTrDUtUrdTeHVXVyi8POqurJTccS7atHn3kVjz5z5w+vSkcUlY4oHvnqt7c9juewiHYu\n6BkWAEBERgHcLiBlRAQAMqp6wb9eERGRn0IRsACcvl0G5QUpETktIqONZFpTU1MYGVlfqiqbzYZ6\nQUjaX1ut5/fj720/z+axL7vvSkkbtEj99ce1+7xiijfHv4Wnh88BAFZ/5/P4ra99BwDwN3/29ds+\nby6XQy6X824ONfxLELWgsASszFaN7twVVPUSgBkAw408WTqdDvQGZkSe+i9TIjLd5O4QNVXgA5aI\nDGNTsYWIpFQ1D2DS/QDASQC3VBES7VR9ZlWfHW1u80rWl579IorH7b9S/+QaAKB87QM8/uQrAIBo\nTwIA8NAffAfvTG5/bouIbi/wAcuZ3XT7KoBTqjouIlkRmQUwoarjTegbtZDNxRCbq/kef/IVYKjT\nboyeAgD85LvnakN91VQPAGB19BR++IM/uu3z+7UqO4e/KQwaHfoOfMBylYHPb2o7VXc9d8uDiAgA\nh7/9wnUF96bRoe/ABywiP9VnPvVl7F4G1Qkg/4VjAIBIyTY/fSrzEsp3DwAASv1xAMBbf/lC7TGe\n709+m/tdEe1BoOdhEREReZhhEW3irVbx47e+uV444TKoq299s5Z59f3NJwCAG08ex8C7KwAsswLs\nXFX+nz8CABj4u3n/Ok/UwhiwqK15w3bLnz0CAIjPlxD50TsA3NwqN69q4T6r9HvisZex9JtdAICV\ngeMAgKHcT2rP97lzfwIAOPylh5D6c2t/k8OARPuCQ4JERBQKzLCorc3+thVQpN5dAACUexJY+N0v\nAgBW+wTVuAAAej+qAAAW7u/EoX9YtfsHrMCi8JWHUe604w69VwIAxJbWkHfPQ+HCBW+DixkWERGF\nAjMsajv15eadD6QBAB+NpgAA1Thw4v/5CABw/ct3I1aw0vVC2r7bSQWY/s0kAKD3wyoAYPlIBEf/\n43V7vJs4zK1CiPYfAxa1hdORs4iduBfA+jyquc9E0DFlASk5Z5drKcH1L98NACj1AFK1ob61Pnue\n1EQVq4PWlpy14b/5+5MMVEQ+4JAgERGFAjMsamne2n3VLz2EGw92briv50NFdM0yq/wD9t2t0qVQ\nd3/fJLB4vzv2V3Y589kIImsbX+OeN/O1zKp+YVyuakG0v5hhERFRKDDDopZUWz39Sw8BsBUqvA0T\n1/rsHFQ1DqxZrQW6btjlcr/WnqM4GEFsya6vHrLLjun1EndvNfanh8/VVse4ynNYRAeGAYta0vfd\nvlNeIHnkq99Gh7tv5YgFrHKnQiz2YPmYBapISWptlU5F1f0PiS/aY1KTpdryS7Xhv2sf4Cr3uSKH\nK7cfnMAPCYrIeXe55SY+InJGREZF5AV/e0ZERH4KQ4aVFZEz2LQnFgCISAYAVPWKiAyLSIabONJT\nmZcQydtYXvG3jgIAyknB7GftflHLpso9VWjMrkcL9t1NVVBJurai1DKs7k+srfP6ci1r8/7zsLiC\nyB9hCFhnVfXKbe57DsBld30SwCgABqw2lfl9O0cVeagflUQ/AGDhpN1XjSs0YUFHkzbhV5IVyIIt\nr1Tps3HA2FysFsSkLOi+4eZczdv9b45/a73y0L3u6chZBi0iH4QhYGVEBAAyqnph030pALN1twd9\n6xUREfkq8AHLC1IiclpERrfJtho2NTWFkZGR2u367ZkpHLwsp371iqS779MRgUYtS6rGXVbVW4a4\ntniiDAAoLSaBDldhUbYhwXJ/GZFC1NoESM67xyzaY5547GVcddmU14eDzK5yuRxyuZx3c+jAXqjN\nccHbcAh0wHLnrqCqlwDMABjedEgewIC7nnLH3FE6ncbY2Nh+dZPowNR/mRKR6SZ3h6ipAh2wYOel\nJt31kwAuAoCIpFQ1D+B1AF6qNAxgz9kXhYM3v8pbdCJ/MoLCvZYFSVUQWbFzT955q2iiimSHHV2t\nWjYVTRWxWrBzWGqHQ1aiiBXsRmJB0DVl6wXOfcbyt8P/x1/5klkR0a0CHbBUdVxEsiIyC2CirgLw\nKoBT7v4RERkFkGeFYHt4evgcSm6H4Kl/bAFnLaXw1lTSjgrUK6Jww4CH+pdRKttQ32rJTRx2C9sC\nANzVaCGCxLxb8LZfkR+25x/4xWrtUAYqouYIdMACAFXNbdF2arv7iYio9QQ+YBF5vJUlZh4/jsUT\nlgWtDriiiSiAhBWaJ3rWkEzaUJ6XQ5XKUYi4rUTiNnRYUUFp1f0XWHSFFlUgYg/FA69dx4rbLys5\nvQIAeJPZFVHTMGARtTBWxFIYNFoNy4BFgeItWvv9urX5Hv49u174H22l2lIXUOpx03b7LR1KdpUQ\niVhbR6KEaMSyqY7YejZVLNnbvVSxbKpcjkJnrZjCm0wcX5TaZo4rD6RrmZW3ckbYsCKWwqDRalgG\nLAoUL1B5w3+R/BIKX7EdgNf614/TI1YEkeywgNSZXENnwoJXR6yMQ8kCAKBYsaKJmZWuWhCrVKxK\nsDjTCXFtPb+0/wo9H1dRSdhAYvzK28BDD27o19PD5zYEUyLyT+AXvyUiIgKYYVGAPPHYy4j86B0A\n66XjD/3Bd7B83IbrKinLpmJdZSTdahVH+hcBAIlIBVVXYtEZKyHmhgcjVVfW3rGCm0s91hZZXyvQ\nW/TW1WOgGhX0fLS2oQ/1mF0RNQ8zLCIiCgVmWNQUW61wXkwnga88DAD4zf/lTwAAi7+mqAzYuanu\n/iIAIB6t4HCPFUEMdiwDAJLRMipuuYqqRlCs2Ft7uZQAAKyWYyiu2fmswlQ3AMuqvBL2stvdsfuT\nNTt3VddPgJOFiYKAAYt8tVUAePQZW3i0GheUuizoLN3rCiS6q+josSG6hKv4O9S1gmPd8wCAxZJV\n+cUiFUTduF5UKlirWiVg1QWxtUoUhflOe8HaihhVdPzSjuuasiHE1YE4fljXNwYq2ovNi+pyB+K9\n4ZAgERGFAjMs8pWXsTz+5CtYHbAhumLavjctngAqnW5bkE7LeO66bwbJqK1m0ZOwUvbhnvVpGgNx\nGxJcrcbRGbVM7MOVQyiUExted7UUgyxbNiVltynjdATxJXu91LsLAMI734qoHTBg0YGrHwb0rnee\nuBf5f3ocALB4vx0XWQOqqY3nq9JdyxhMWlCKiQWxvlgRcbEg5lUGxqrV9TYVfLpsFYHLRQtckYgC\nbrHbxJwFyFgRSC5awFq6vxcA8OPvfWt/f3ki2jcMWETUdrhhYzgxYJFvHn3mVSx/7ZHa7cJRV9Xn\nFq2tHF3D0IANyR3vtaKKjlgJh5M216o3allXV2QNxaoNJ67q+lv45mpf7XrcDSN6c66WZruQdNuG\nxOxp0PthFV0f243ER7MAgMe+fAFv/eUL+/L7EtH+YtEFERGFQuAzLBHxlpY+qaovbnH/eVV9UUSy\n3BsrmLzdgX/8vW/gsy/Y/KpyD1DqtcxKB61Yor+/gMEuO1+V7rBM61hHHv1RtwCtO4cVhdYyqGjV\nzlEVKgksu0KLhbUOLKzYxKqlaZtzFVmKIup2Ib7rr2ydwatvfbPWR+/c2ltcyYIosAIdsNxOwldU\ndVJE3hCRUVW9sumwrIicAfB8E7pIDSj12/Dd5879CcTl9GupKpJ3W1Dq67JhuWM987VANeiq/w7F\nlpGKuoVs3TBgXCooVG3+VcEFrHypC0tla5ta7kZhyc3PmrG3eGJekJqwgDf3GZuP9cRjL9eCFudb\nEQVf0IcEhwGMuuuT7vZmZ1X15BaBjIiIWkigM6xNQ3wZAK9vcVhGRAAgo6oXfOkY3Za3asWPv/cN\nPPJVG17L/2PLjMo9ikrSiiC67ltAX6dlVoOdlkGd7JnGoZhdH4hZptURKaErYvOvOsRK3vOVLhRd\nsUXELVtR1ghmVroAAMsrSUQ+sSFBb+mlno8UPX/xUwDAT5hNEYVS0DMsAICIZABcVtXxzfep6gWX\nXQ26IcQ78nZh9X7qdrokCpRcLld7n2KbnViJ2kGgM6w6o1tlT+7cFVT1EoAZbD1keAvuwnpwuv7D\nzwBYefjMw975I7tvLQrE77NzU8f753G408rVU3ErqjienENXxAowDsds5Ym4lFF056kW3XkrALi+\neggAsOzOW11bHMB8wc5NldZi6PuVW0PQVbpHKtVQnqdqdCdWonYQ+IDlqv8uuOujqnpFRFKqmoed\n15p0h54EcLFZ/Wx33tb2npnPxuFiDxY/Y+NyPell3NVnQWqoYxn3ds4BAO5O2Byou+LztWG/ilvB\nArCgBQBRVyU4W+nGittJ+IMlC1wrpTiW5yxgRWfjkIoNFQ78V1dZuFat9ZF7WhGFU6CHBN0Q33kR\nmRCRubq7rgKAGyJ81mVaE1sNGRIRBYXIq7Uf2rlAZ1ju3NShLdpP1V3nCagmOx05i4Xf/SIAYOnZ\nuwEAa72KtUGbK9U1aIUU9x2aw4luy6bSiUUcidtY4V3uskPWCyyqat+lljWBolo2teyGBKdLvfhk\npR8AsLBmbTdvpCAFW9w2Ul7vmzdEWb+OITMtonAKdIZFRETkCXSGReGw8LtfxNIx++6z1m/njtYG\nK+i9y85X3ZvKAwCOd+VxX6fVDRyL55GOWmFFh6s975Y1lNSypKL7LlXSGPIVW61irmyXn672YtFl\nVlNztso6ihH0vWeP7b5ZRXzZsrvL3IyRqGUwYNGueUNsPV96CLMPugq9fitySN8zhyG3zNK93Xb6\n8f7OKRyLu+AVm6ubX2XBpaSRWrHFmgtcM+We2moW7xesqvuj5RRml23OVXTCXrf/Q6Ajb6/9k++e\nq/Wt3XlTODz1VYdEQZHL5eqnF912+gYDFlEL4xQOCoNGp28wYFFDvIwl8tCDtV15p7O2VcjCMFDu\nsywpMWirVxzuXqrtDHyiYwYAcCw+h2Nxy7b6ZLX23HFYZlRCBCW3gkW+ahnUdLkXvywcBgBcWxoA\nABRKcSx+aBOsOlYtI1s+DsRsOhcef/IV/JDDf0QthwGLiNoCS8nDjwGLGhJ56MHa9ZUH0gCA+V+z\n25WeCnqPWoHF8CErW7+3exbDnVPWlvgUAJCKFmqZVVekjKhbB7DozleVNForYZ8qWwb1yVo/Ft1q\nFgtFWx9w+mYfImXLrNzpL/R9pOh7321D8qN39u33JqLgYMCihsx+zuY9RSqKmc+5nYIPW/Dp6SvW\nKgG97UF+o/t6bXmlu2LePKsKUm6SVARAQe15vEKLfLUL10s27e6TtRQAYL7UiY+X7LXzCzZMGJmP\nITlrVYTxpfU+eoGK1YBErYnzsIiIKBSYYdEtvAKL0qgtKDL3mSTKR125eT9QPmaZ1eCgpTdHehbx\nQK8N/53ssMvj8VmkIrbCRa9bHzAVqcKN4CGK9aHAfNWG+haqHZgu27yqT4qWVb2XT2N20eZfVeZs\naLBjJoL+961Qw1vVou+9BbzJzIqopTFg0QanI2dr56viV94GAFQ/+9tYTdn5plK6jO5eC1j39FnF\n38meaZzssPNU9ySsIvCu6AIG3Oq33RELdvG6hP7jiqDgdhDOV2yo75fFu/D+ik3BeHfuCABgarYP\nlaIFts6P7VKqwM/+3blafwEwWBG1AQ4JEhFRKDDDog0uV99A5ve/AwAo//e/DQAoHlZU77P5VYd6\nC7i33wosvBUsTnZ8Wsus0lGrFuyNlDAQtYwoCq+4QnGzYkN5i9UkrpetwOJjV2jx8WoKv1ywDGtx\nxYYJKytRdP83W+kiMW9ZXudaxpSlAAAZdElEQVSsbugvEbUHZlhERBQKO86wROQZAK/Bdvh9RVX/\njWv/uap+fp/7Rz47HTmL0v/qMqshy2SqMaC72zKswe5CrXT9wa6PAQB3xfIYjNi6gWm3PmA6GkOn\nWGa0qlYZMVVZq523Wqh2YMoVWPyicBQAMLk4iHmXWa0s22OT1xO3ZFY/+e7GzSKJwqh+IrPqN5rY\nk/DYzZDgKwDuV9V5ERkTkTFV/VugbovYfeQ2Z8wDyHg7D+/kftqetzdU+doHAGy5pcJxCwzllAWa\nZKqI+w7Z8N/Rznn8d92fALBAZZfzuCtqgepI1Cr5khLHqlp14JK7XNQYPnDDgDfKKUwUbcmlD5at\n7eZiLxaXLWAl37NFbe//849q+1Y9lXmp1m+v2IJDgkTtYzdDgu+r6ry7/iyAf+2u622O3zURyQC1\njRzz3u1G7yciotaxmwxrUkT+TwDnVXVSRN4QkR8AGN7nvgHAcwAue68LYBTA+A7up208PXwO+S8c\nAwCkXNtKWlBOWUYU77Os6a7UIu7tsgzrga6btQVsD7sCi3RkFf0RK7BIig35lbSCxaqVtd+o2H03\nyr24UbZXulYcwsSSFVh8kLe2pXwXMO8e323ff248ebzW3zfHv1W7zsyKqP3sOGCp6tdE5Im62xdE\nZBwWLPZbCsBs3e3BHd5PREQtYldl7ap6VUT+CYBr7vYVEenbz44dJG5qZ5Y/ewSL99io8I1HLNOq\n9q+hs8+VsPfYShWfSd3EsaRbKzC2iHtj9h0hHbXFZgeiUfRH3AaOamtZzFWLtczqQ5dVTZX7amsE\nvrd0GP8wZeewCgt23gprEaTes/5U3TtzKPdXwJ/t928eHo1ubEfUDvYyD+trInIawL+CnceaAfDv\n96VX6/IABtz1lHuNndy/pXbc1O6pzEu1IbVHn7HqpMLxGMQtbaQJG4KTWBWH+6wK8GSf7Wc13DmN\n+xLe1vZzGIhaQBty86y6JLEhUAE2DOgFqusl+xN9spbC387bEN/NQi/WVm34L37TKgJ7rgHpP/sr\nAHVDfn/yh21dYNHoxna0NW4p0lp2PQ9LVZ+FVQbOAfhrVf2X+9arda9j/dzYMIArACAiqe3uJyKi\n1rPrDEtEXgFwP4AHAFwUEajqvn6dUdVxERkRkVEAeVX1CiquAji1zf1tzytX90rC3xz/Fh75ql1f\nOGmZUXxJUbKpUIgcsgKLY4PzONxlxRS/3n0TAHB3Yra2RUgqUkTarQ3YVZtnVcJi1VK1fNW+A01V\nujFT7gEA/LcVWxfwV4UBXMtbtpWf64bM2uM75u35+j4s1dYxrO9/O2ZWRHSrvQwJTqjqH7nrT4rI\n/7YfHdpMVXNbtJ3a7n4iImo9uw5Yqvraptt/vPfu0H7xMivP40++guphO2cUsap15H+jCu2wc09D\nfVZgcbhrEQ902xYhR+Iuq4ou40jUzmt1R6qIi71tCmpl64VqBVOuSmLWrbx+vXQIH6xZjYC3VchM\nsRtzN6w2Jz4TQyJvmVXHtJ0/e+svX9ifX56IWhIXv21Rj33ZFv3wgsDqQBzLR224bvluW4BWE1Uk\n3VyrIz02DHh/9wyOJ22eVVdtmaVFdIg9Jg6g6AosCmqBZrYSx0zVApW3tf3fFe7B7JrtY3VtwRVd\nTPdDVmw4svsjQdeUPWe17l3YzgUWRLQ9Ln5LREShwAyrBT09fA5vbRoSTM6WsHDC1vmTI948qxU8\nMGCV0kNJW7z2eHIOJxI2JOgtaJuKrCFat1Kkl1lNVaxooqgxfFiyOdsfrVk2NbE0hL//2Ba1La3Y\nUGTsZgLxJVewMVVFfNkytfqhQGZWRHQ7DFgt6PuT365V2X3yT23e08JTCVSGbIivu8NOYqW7l9Ed\ns/NQv951AwBwT3wW3WJt/W5IMA5Fya0UWYQgX7VA5e0UfKPcXwtU78zfba+73Ad1FYMRt9xS102p\nna/6yXfP1Yb/6OBwkjyFQaMT5BmwiFpYO06Sp/BpdII8A1YL8QotOq8v48PnLNPxMhqNKrr7bSjw\neL9V//1a3xQGE1b9N+CqAHsjK7Vii6rbMaYEQVGtWKKoUcxWbH7VjbJV//2yeASTy/alaKlkw46f\nzvQBU3Y9MWeZ1qH3Svv/SxNR22DAIiJqss1LSHFDx60xYLWQzutWJJF/sA9dN13J+efcrsH9ZaR7\n3VyquGVQfbEVHE9YCXsqavOwOiK3ZkFFjWLRnbeaqfTUtgj5YNUKLSaXh/CLaVvNYrlgWVXXf+6E\nWxsXR/7U1geMnbh3w/wwFlgQ0U4wYIVc/XyrleM272n5aKQ21yp61AJRd0cJR9ySS8c7bOX1ofgS\nUlELct1uGLBDSii54b9l9xr5ShcWq7Ya+8elQ5gopgEAHxUscH20mKrtFBx/zwoxVICBX9hzMjAR\n0X7gPCwiIgoFZlgh1/1fbIHapzIvYf5Ry3i0bs5UPG5znfo7i+iP2xjd4YRlWv3RAvoiVohRUfvu\nkq901bItr2w9X+2qrWAxW+7GpNsp+MaSrZw7O9+N6lxyw2sn5xQ//IEtNcnVK8gv3E6ktTHDIiKi\nUGCG1SI+/UI/ilYDgXKPovM+y6KO9NnlcO8M7kouWFttUdvCLc9TRQQzrmzdK19frHRuOG/lZVb5\nRTuvpTc60DFj333637dzZz/7d+dqz8nMioj2AwNWSG1eyWL5HmBtyCr8+o4sIRqxwHG82wosumOr\nOBSzMoo1tT97RSNYQ9Q9o11WEakNBd4s2Tyr95aPYGLBhgFnC51YcZWA+NACVkde0PerWwMVEdF+\nCvyQoIhk3c/529x/3jvO354REZGfAp1huZ2Er6jqpIi8ISKjqnpl02FZETkD4PkmdNFXXlYFALO/\nfQwAEHe155FVASpW8RCLVnB/atbud9uC9MdWakOAEVRrz1NUW+evWld04WVWN9es0OKj5RQWipZV\nLX3Q5yVj6J6x10vOKFLvLuzjb0pEdKugZ1jDAEbd9Ul3e7Ozqnpyi0BGREQtJNAZlqrm6m5mALy+\nxWEZEQGAjKpeaOR5w7SC9ROPvYyrb30TAFC+9gEAoPqlh7DWY9nNwkk7rjy0hp4By6BSnUXE3Dms\ngYSlYEOxpVrpesRlXTOVntok4aWKTfydL3fi5qplVh8sHwIALK4mMT/vJgTHFP2/2Pi2GX/t61x5\n/YA0uoo1UTsIdMDyiEgGwGVVHd98nxekROT0bYYMbxGmFawjP3oHv/W17wAAOp79IgBg6WgEy3fb\nkkuVVBkAEO8qoafD5k8lo2Xc02lLLiUj5dpzzbtiCm9x26LGsVq1IcHJFfssXKvG8F7eKgLnC1ZU\nsfxpNxJTFthEgVjBXnv8ta/XnpuVgAej0VWsidpB0wPWbYolJjcFntGtsid37gqqegnADLYeMiQi\nohbQ9IC1adjvFiKSrcuiRlX1ioikVDUPO6816Q49CeDiwfb24D3x2MsAgNiSbaJ4ufpGra1wrMNd\nKjRmWU68x2222LuCnoRdH0gWEBG7vytibYVqAnGxVS+8TGux0lEb/vtkxS5nVrpQrlg2tfKBzbfq\nnIqgY8a93jLw839rmZVXBPL9TbsbE9He1K/YwZXb1wW66MJVCZ4XkQkRmau76yoAuCHCZ12mNbHV\nkCEREbWGpmdY23HDgoe2aD9Vd33bDC1MTkfO4qo7F+RlLw/9wXcQ+YydS1q61wotNFatrcJ+fNBW\nrShVohjscCuvx1Zr2VTBbQsSlwoKsOtLZStRL2kUy2Vrmyvaa+SXurC6Yue1okVXtp4Hjv7H67f0\nl5kVEfkp0AGr3dQXLlRTtixSfBnonLXCicX73Z+rKohGbYhupWTB5e7ePKpu5dmoaK36L+rmXJUQ\nRb5kQ4Gza24li5VezLnCirkbNiQoK1Ek3Q7BR39kxRk//MEfAX9qL81qQCJqlkAPCRIREXmYYQXM\no8/Yydbqr1vGUzgqmP0N+zOpy6r0yCo6ErZuYG/SsqCIKI512vDgSiWOlUp8w/OWNYrpVdvg8dOC\nFVMUSvHa/CpU3SoZK4KYWxM3WqzUHv9U5iV7nYce3KfflGh/cEuR9sGAFSCnI2dRdXOtPJFVAG6P\nqUq/BZBksoyImxjsDQNGRGvDf8vlJFZkY8D6pNCPxVU7d7Vasj97fqoHiRt2XI/NSUapV/D3f/yH\ntf543hz/1n78ikREu8YhQSIiCgVmWE329PC52pJLkYcexGqvZUyrg3a5ktbaahaIWQaVSJTRGbe2\nmFtmqVBO4JOiLVpbhaBYsmxq0V0uFDuw6LYFWV22ysCuiQRKfbqhP52faq1CkatXhF+YliGj9tXo\nEmQMWEQtLEzLkFH7anQJMgasJqufy/TEYy+j3GWZVXHQMp9qQiFrNnIb77MCi76OVcSjdj5L3IoW\nESiKFftzTq/0YM2tVjGdt/L4ajkCXbW22KwdF18CBv6rZWhd/+FnAIDV3/n8LfOrnsq8xHNYRNR0\nDFhN4hU0lEZP1arxFu7vRMniS23SbvnwGqJJuz+RsGFAxfr8q2LZ/oTF6HpFX1UFMy5Q4WNbzkmi\ntdoNDPy9XWpUEV+059xu+I/BioiCgAGLiCjAuK7gOgasJvEymseffAXzD9hqE5UEsDrg5lq5xW2l\nEEPVzb/qcIUWCysd6Oss2v3u+WYLnVhxRRXRWAX6iWVWbgsspH4hWOtzBR0pa/vP//sfHtBvR0S0\n/1jWTkREocAMy0f1JeyezhP34vr/cBwAEF0VSMXLsOx+6VtDd69lU6tlK5pIxMoorFpp+rLLqmLx\nCipzdl2LAqlYNtU5bZelXuC/XLCMytuuhIgoTBiwfPT9yW/XgoW37f3n/8V3kJh3c66OVmvHVjvt\nuuQTWHbjfhJxlYNrUcQ6bGkmdStdFAsJoCy1xx961y7jBXuevvcWbnntx598xRa2JQoZLsfUnjgk\nSEREoRD4DEtEzqvqi27n4Vv2vnKbN+YBZLydiYPq6eFzwN0DANYXk9WH+rFyxBVYlAQat+uxBRv+\nK6fK0JL7XrFibdJVQXnV/enm3d5VKxF03XCZ2mHFots7q+99O6y+NN0rqU9yIVsiCpEwZFhZEZkA\nMLn5DhHJALWNHvPebSIiaj2Bz7AAnHUBaSvPAbjsrk8CGAUw7kuvdsDLaApfeRgrA/YdoRp3peyd\n69t5aFSxaVcQIF4FXAEFku4c13IMsuZ2A5615+u9psj/I8vODr0LrBy2Qwf+bv6W/nCNQCIKozAE\nrIyIAFsP+aUAzNbdHmzkCf1eEDR24l4AQHyxjJ73VwAANx61yVBrvUC5xwJRbCkCVDc+VhbiUBeo\nEjNumLBLEV/0ApYdVzgq6P7QAlbf+yvonLXIV33n3QP6rcgPjS4KStQOAh+wvCAlIqdFZHSbbKth\nXBCUwqLRRUGJ2kHTA5aIbJXaTKrqFVdQAVW9BGAGwPCm4/IABtz1lDumKbxhv+2G25aPJrBwn82f\nKtxl2VB8SSCz66cSq4dciuXK1RFT9P3C/kzVpGtaEnR+ao/vnF1fQ9BbwLY0egqR1U2pGhFRyDU9\nYG1V+VdnEuvFFicBXAQAEUmpah7A6wC8sb1hAHvOvnZru0C18FtHAQCRiqLYY8EpulZ3gItNGnHD\nggA0itplqc8d57au6p9cD0Z9f/MJAGDlgXStD6cjZ2vDkN/n+SoiahFND1jbUdVxEcmKyCyACVX1\nCiquAjjl7h8RkVEA+br7iYhaTrsvhBvogAVsnYGp6qnt7g+aNZdVFYcEx/5THgBw7X+yoguNrC/D\nVI0q3AbCiJQs7RKVWiFG1w1LsZLzFSzdZQ/ylnqK1y35xCpAakVc3YLCMA+LiIgo+BlWWD3+5Cu1\njRm7O+yE1OK9yVo5e/d1y5ZWDguSrlQksQjM/5pd7/zULtf6gNSEpViRkj2mkI7hyNXrAHiOioja\nBwPWPnl6+ByAuiE6WLUeYNWBAJBYAArHLOgk5mzI79j/V8C1f2aTiFcLgq6P7fkO/cMqAKDUG0PP\n+4sAgEh+CYAVWGzexp6IqNVxSJCIiEKBGdY+OB05i4hbSNYreHjisZeRnLZVLVYO26oTnTOKI3/6\nUwDr2df1x7tweNyG/EpdwKH/6ycAgPw/fwQA8PN/+3U89mVb4OMtt4CtN+eLiKidMMMiIqJQYIa1\nDy5X38Cjz2wsuS2mk1h9wM5NdU2VAQCR1Sqms5Y5Hf5rW5Q2OZfE8hH73pD+m0Lt8d6itU8Pn8Nb\nm85XsWydiDaX+bfDvCwGrD14+PcskCTnK+i5vgxgfbiu40sP4cff++aG4x995lUMvGvDhJ9+oR8A\n0H+thNl/ZEOGkR+9UzvW278q8/vfOcDfgFqd3ws9E+1Go4s8M2ARtTAu9Exh0OgizwxYe9D3f/+0\ndv3NunX8AODqW9/E5/+FZUexVStl/8n3voFHvmpZmZdpFY514O//+A8BAI//7Sp++IM/2vAa4699\n/QB/A6Lg4soWtBmLLoiIKBSYYe3C5rLyy9U38MRjLwMAYq68/ZGvfhs//65NJvYmFZ+OnEXKraK+\n/NkjAICev/gpnvjYHptcWgMREW2NAWufXH1rY4HFw7/3bTyVeQkAUL7btuwqfOEYev7ChhG9yr/H\nn3ylNgzoBTYiIroVhwSJiCgUmGHtg9ORs7WVKVJ/bitVxL7yMKrvvAsAuOoKMh778oVb5lB5C+QC\n4PqARLRr7bBXVqAzLBHJiIiKyIT7ubjFMefdJSeXEBG1sKBnWAOqKoAFLwD5LY7JisgZAM/70aGn\nMi9tudKEV4jh3ffw730bq7/z+Q3HvPWXL9zyuKtvffOWxxK1K5ay03YCHbBU9UrdzWFVvbTFYWc3\nHXegvBUoNvOCjVdo0ffOuw0HIAYqIqI7C3TA8ojIKIDbBaWMiABARlUvNPJ8XK6GwqLRJWuI2oGo\narP7cEcicl5VX7zTMQAuN5JtjYyM6E6Wq6mfd3W5+kbDQ3jecTE394pFFbQXIvK2qo7c+ch1O32v\nNxuHBPdf2AowtnufNz3Duk2xxOSmwJO5zWPPAIAbKpwBMLwfffLmQ1VTPQA2BqnTkbMND+F5e2R9\n/zbDiERE1LimByxVzW13v4gMY1OxhYikVDUPYNL9AMBJALdUERJRsDGrokY1PWA1aHbT7asATqnq\nuIhkRWQWwISqju/ni9YXWNTvJLybxxMR0d4EPmCp6iQ2layr6qm669tmaERE7ayVJhQHeuJws5Sv\nfYDytQ+2vG/zmoEA1wAkIvJD4DMsv9RX/t2pqMIbFvSCF6v/iHaG561oN5hhERFRKDDDcnay2sRW\nw4JEREG3ObMN2zktBiwiOnAcAqT9wCHBHapbJicQgtYfIHh9Clp/gGD2ab+JvFr7udVPfe/P/gpz\n/9f7vv3fKHgYsHYoaB80QesPELw+Ba0/QDD7tB8a/wD8mS/9OThh7v/WfQ9D8OKQIBHtSZA/4Gh3\ngnquKxSL3+43EZkC8KtdPnwIwPQ+dmevgtYfIHh9Clp/gN316T5VTe/kASKyiI0jKVO7eN2DEMS/\nyU6Euf9B7PsQAO+9XVXV3q0OasuARURE4cNzWEREFAoMWEREFAoMWEREFAoMWEREFAoMWEREFAoM\nWEREFAoMWEREFAoMWERNICIvNLsPRGHDgLUP+OFjROSMiIwG6d9DRLLu53yz++IRkVEAn292P2jn\ngvge3wnv/4GIZJvdl91gwNqjIH34NPPDWUQyAKCqVwDkvdvN5P42V1Q1B2DY3aaACcuHaBDf47uQ\nFZEJAJPN7shuMGC1iAB8OD8HIO+uTwIIQnAYxno/Jt3tphKRjPvAo3Vh+RAN4nt8p86q6smwvgcZ\nsPYgYB8+zf5wTgGYrbs96PPr30JVcy6AA0AGwFgz++MMNLsDARSWD9HAvcd3IRPmIU1uL7I3gfnw\nqftgBuzD+fVm9SVo3NDNZVUdb3Y/QvCh3AwZEQGAjKpeaHZnWpn37ysip0VkNGzvRwasbdxmTH1S\nVa8E9cOniR/OeawH8BSAGZ9ffzujAfkgHBaR4brrmWYH0SAI0YdokN/jdyQiZwBAVS/B+t70IfKd\nYsDaxqasZTPfP3y2C6B1t5v14fw6gBF3fRhAID50RCRb94HY1A9D90Hh/R1TzeqH3+7wxS9MH6KB\nfI/vwCTWzxOeBHCxiX3ZFe6HtUfuP+OLsHH4Zg85Zb0g24wPZ/dvMQlg+A7B3q/+jAJ4A3beYQD2\nNwrbh0xLcyMCk6qaF5GLAC42+//RdoL2Ht8p1/9ZWP+DMOqwIwxYLYIfzhRWYf8QJf8wYBERUSiw\nrJ2IiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuIiEKBAYuI\niEKBAYsOhIhkw7pJHFEjROSiiFwWkQlv1Xk6WAxYtO9E5DJCuHUBUaPcYtNQ1dMATgF4rbk9ag8M\nWLRrLos6766/sek/8fNN7RzRPrnN+3wSwHkAUNU8bLV5OmAMWLRrbj+gYfefefNGkkQtYav3uapO\nquqkiAyLyNtwwYsOFnccpr16EcAEgEPN7gjRAbrlfe7O0T4H4PeDvOlkK2GGRXt1EfafmWP41Mo2\nvM/dsOBpVT3FYOUfBizaNfcN87LbJXaWlVLUim7zPj8NYERE3vZ+mtvL9sAdh4mIKBSYYRERUSgw\nYBERUSgwYBERUSgwYBERUSgwYBERUSgwYBERUSgwYBERUSgwYBERUSj8//Mqev6u+PB/AAAAAElF\nTkSuQmCC\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x1139980b8>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plotChain(chain_full, param_names)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
